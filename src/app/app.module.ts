import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppComponent } from './app.component';
import { LibraryListComponent } from './library/list/list.component';
import { LibraryCreateComponent } from './library/create/create.component';
import { LibraryUpdateComponent } from './library/update/update.component';

import { AuthorListComponent } from './author/list/list.component';
import { AuthorUpdateComponent } from './author/update/update.component';
import { AuthorCreateComponent } from './author/create/create.component';

import { BookListComponent } from './book/list/list.component';
import { BookUpdateComponent } from './book/update/update.component';
import { BookCreateComponent } from './book/create/create.component';

const appRoutes: Routes = [
  {
    path: 'library',
    component: LibraryListComponent,
    data: { name: 'Library List' }
  },
  {
    path: 'library/create',
    component: LibraryCreateComponent,
    data: { name: 'Create Library' }
  },
  {
    path: 'library/:id',
    component: LibraryUpdateComponent,
    data: { name: 'Update Library' }
  },
  {
    path: 'author',
    component: AuthorListComponent,
    data: { name: 'Author List' }
  },
  {
    path: 'author/create',
    component: AuthorCreateComponent,
    data: { name: 'Create Author' }
  },
  {
    path: 'author/:id',
    component: AuthorUpdateComponent,
    data: { name: 'Update Author' }
  },
  {
    path: 'book',
    component: BookListComponent,
    data: { name: 'Book List' }
  },
  {
    path: 'book/create',
    component: BookCreateComponent,
    data: { name: 'Create Book' }
  },
  {
    path: 'book/:id',
    component: BookUpdateComponent,
    data: { name: 'Update Book' }
  },
  { path: '',
    redirectTo: '/book',
    pathMatch: 'full'
  }
];


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken'
    }),
    AngularMultiSelectModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true, enableTracing: true }
    )
  ],
  declarations: [
    AppComponent,
    LibraryListComponent,
    LibraryUpdateComponent,
    LibraryCreateComponent,
    AuthorListComponent,
    AuthorUpdateComponent,
    AuthorCreateComponent,
    BookListComponent,
    BookUpdateComponent,
    BookCreateComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
