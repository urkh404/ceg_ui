import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-author-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AuthorCreateComponent implements OnInit {

  author = {};

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  saveAuthor() {
    this.http.post('/api/author/', this.author)
      .subscribe(res => {
          this.router.navigate(['/author']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
