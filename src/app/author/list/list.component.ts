import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-author-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class AuthorListComponent implements OnInit {

  authors: any;
  next_page = null;
  previous_page = null;
  search = null;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.fetchList(null, null)
  }

  fetchList(page, search){
    let url = page ? page : '/api/author/'
    url = search ? url + '?search=' + search : url
    this.http.get(url).subscribe(data => {
      this.authors = data['results'];
      this.next_page = data['next']
      this.previous_page = data['previous']
    });
  }

  nextPage(){
    this.fetchList(this.next_page, null)
  }

  previousPage(){
    this.fetchList(this.previous_page, null)
  }

  onSearch(){
    this.fetchList(null, this.search)
  }

}
