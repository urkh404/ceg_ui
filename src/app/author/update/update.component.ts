import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-author-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AuthorUpdateComponent implements OnInit {

  author: any = {};

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAuthor(this.route.snapshot.params['id']);
  }

  getAuthor(id) {
    this.http.get('/api/author/' + id + '/').subscribe(data => {
      this.author = data;
    });
  }

  updateAuthor(id) {
    let data = this.author;
    this.http.put('/api/author/' + id + '/', data)
      .subscribe(res => {
          this.router.navigate(['/author']);
        }, (err) => {
          console.log(err);
        }
      );
  }

  deleteAuthor(id) {
    this.http.delete('/api/author/' + id + '/')
      .subscribe(res => {
          this.router.navigate(['/author']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
