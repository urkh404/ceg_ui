import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-book-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookCreateComponent implements OnInit {

  book = {};
  libraryList = [];
  authorList = [];
  selectedLibraries = [];
  selectedAuthor = [];
  librarySettings = {};
  authorSettings = {};

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    
    this.librarySettings = { 
      singleSelection: false, 
      text:"Libraries",
      enableSearchFilter: true,
      searchAutofocus: true,
    };

    this.authorSettings = { 
      singleSelection: true, 
      text:"Author"
    };

    this.http.get('/api/library/').subscribe(data => {
      this.libraryList = data['results'];
    });

    this.http.get('/api/author/').subscribe(data => {
      let authors = []
      data['results'].forEach(function (element, index, array) {
        authors.push({'id': array[index]['id'], 'itemName': array[index]['first_name']+' '+array[index]['last_name']})
      });
      this.authorList = authors
    });
  }

  saveBook() {
    let libraries = []
    this.selectedLibraries.forEach(function (element, index, array) {
      libraries.push(array[index]['id'])
    });
    this.book['libraries'] = libraries
    this.book['author'] = this.selectedAuthor[0]['id']
    this.http.post('/api/book/', this.book)
      .subscribe(res => {
          this.router.navigate(['/book']);
        }, (err) => {
          console.log(err);
        }
      );

  }

}
