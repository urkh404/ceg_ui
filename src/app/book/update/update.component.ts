import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-book-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookUpdateComponent implements OnInit {

  book: any = {};
  libraryList = [];
  authorList = [];
  selectedLibraries = [];
  selectedAuthor = [];
  librarySettings = {};
  authorSettings = {};
  

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getBook(this.route.snapshot.params['id']);
    
    this.librarySettings = { 
      singleSelection: false, 
      text:"Libraries",
      enableSearchFilter: true,
      searchAutofocus: true,
    };

    this.authorSettings = { 
      singleSelection: true, 
      text:"Author"
    };

    this.http.get('/api/library/').subscribe(data => {
      this.libraryList = data['results'];
    });

    this.http.get('/api/author/').subscribe(data => {
      let authors = []
      data['results'].forEach(function (element, index, array) {
        authors.push({'id': array[index]['id'], 'itemName': array[index]['first_name']+' '+array[index]['last_name']})
      });
      this.authorList = authors
    });
  }

  onLibrarySelect(item:any){}
  onLibraryDeSelect(item:any){}
  onAuthorSelect(item:any){}
  onAuthorDeSelect(item:any){}

  getBook(id) {
    this.http.get('/api/book/' + id + '/').subscribe(data => {
      this.book = data;
      this.selectedLibraries = data['libraries']
      this.selectedAuthor = [{'id': data['author']['id'], 'itemName': data['author']['first_name'] + ' ' + data['author']['last_name']}]
    });
  }

  updateBook(id) {
    let libraries = []
    this.selectedLibraries.forEach(function (element, index, array) {
      libraries.push(array[index]['id'])
    });
    this.book['libraries'] = libraries
    this.book['author'] = this.selectedAuthor[0]['id']
    this.http.put('/api/book/' + id + '/', this.book)
      .subscribe(res => {
          this.router.navigate(['/book']);
        }, (err) => {
          console.log(err);
        }
      );
  }

  deleteBook(id) {
    this.http.delete('/api/book/' + id + '/')
      .subscribe(res => {
          this.router.navigate(['/book']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
