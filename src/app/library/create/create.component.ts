import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-library-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LibraryCreateComponent implements OnInit {

  library = {};

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  saveLibrary() {
    this.http.post('/api/library/', this.library)
      .subscribe(res => {
          this.router.navigate(['/library']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
