import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibraryUpdateComponent } from './update.component';

describe('LibraryUpdateComponent', () => {
  let component: LibraryUpdateComponent;
  let fixture: ComponentFixture<LibraryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibraryUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibraryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
