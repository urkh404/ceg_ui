import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ceg-library-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LibraryUpdateComponent implements OnInit {

  library: any = {};

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getLibrary(this.route.snapshot.params['id']);
  }

  getLibrary(id) {
    this.http.get('/api/library/' + id + '/').subscribe(data => {
      this.library = data;
    });
  }

  updateLibrary(id) {
    let data = this.library;
    this.http.put('/api/library/' + id + '/', data)
      .subscribe(res => {
          this.router.navigate(['/library']);
        }, (err) => {
          console.log(err);
        }
      );
  }

  deleteLibrary(id) {
    this.http.delete('/api/library/' + id + '/')
      .subscribe(res => {
          this.router.navigate(['/library']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
